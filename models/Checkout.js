const mongoose = require("mongoose");

const checkoutSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},

	deliveryAddress: {
		type: String,
		required: [true, "Delivery address is required!"]
	},

	/*deliveryFee: {
		type: Number,
		default: 50
	},*/

	productId: {
		type: String,
		required: [true, "Product ID is required!"]
	},

	totalQuantity: {
		type: Number,
		required: [true, "Total quantity is required!"]
	},

	totalAmount: {
		type: Number,
		required: [true, "Total amount is required!"]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Checkout", checkoutSchema);


/*paymentMode: {
		type: String,
		required: [true, "Payment mode is required!"]
	},*/