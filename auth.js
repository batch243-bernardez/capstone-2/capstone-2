/*Dependencies*/
const jwt = require("jsonwebtoken");
const secret = "House of Crust";


/*Create token && Payload of the jwt*/
	module.exports.createToken = (user) => {
		const information = {
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin,
			firstName: user.firstName,
			lastName: user.lastName
		}

		return jwt.sign(information, secret, {});
	}


/*Token verification*/
	module.exports.tokenVerification = (request, response, next) => {
		let token = request.headers.authorization;
		console.log(token);

		if(token !== undefined){
			token = token.slice(7, token.length)
			return jwt.verify(token, secret, (error, information) => {

				let isTokenValid;

				if(error){
					return response.send({isTokenValid: false});
				} else{
					next();
				}
			})
		} else{
			return response.send("Authentication failed, no token provided. ")
		}
	}


/*Decryption of token*/
	module.exports.decodeToken = (token) => {
		if(token === undefined){
			return null
		} else{
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (error, information) => {
				if(error){
					return null
				} else{
					return jwt.decode(token, {complete: true}).payload
				}
			})
		}
	}