/*Dependencies*/
const bcrypt = require("bcrypt");

/*File directories*/
const auth = require("../auth");
const User = require("../models/User");
const Products = require("../models/Products");


/*Add products to API by Admin User*/
	module.exports.addProducts = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		console.log(userInformation);

			let newProduct = new Products({
				category: request.body.category,
				name: request.body.name,
				description: request.body.description,
				price: request.body.price,
				stocks: request.body.stocks
			})

			if(userInformation.isAdmin){
				const product = Products.findOne({name: request.body.name})

				let productExists;
				if(product){
					return response.send({productExists: true})
				}

				let productCreated;
				return newProduct.save().then(product => {
					console.log(product);
					response.send({productCreated: true});
				}).catch(error => {
					console.log(error);
					response.send("Error occured");
				})
			} else{
				let isUserAdmin;
				return response.send({isUserAdmin: false});
			}
	}


/*Retrieve all active products (can access by user)*/
	module.exports.getAllActive = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		return Products.find({isActive: true}).then(product => {
			response.send(product);
		}).catch(error => {
			console.log(error);
			response.send(error);
		})
	}


/*Retrieve all products by Admin only*/
	module.exports.getAllProducts = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);

		if(!userInformation.isAdmin){
			return response.send("Sorry, you don't have access to this page.")
		} else{
			return Products.find({}).then(product => {
				response.send(product);
			}).catch(error => {
				console.log(error);
				response.send(error);
			})
		}
	}


/*Retrieve single product (can access by user)*/
	module.exports.getProduct = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		const productId = request.params.productId;
		return Products.findById(productId).then(product => {
			response.send(product);
		}).catch(error => {
			console.log(error);
			response.send(error)})
	}


/*Update a product by Admin only*/
	module.exports.updateProduct = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		console.log(userInformation);

		const productId = request.params.productId;

			let updatedProduct = {
				category: request.body.category,
				name: request.body.name,
				description: request.body.description,
				price: request.body.price,
				stocks: request.body.stocks
			}

			if(userInformation.isAdmin){
				return Products.findByIdAndUpdate(productId, updatedProduct, {new: true}).then(product => {
					response.send(product);
				}).catch(error => {
					response.send(error);
				})
			} else{
				return response.send("Sorry, you don't have access to this page.")
			}
	}


/*Archive a product using ID by Admin only
(isActive: false - out of stock or not available)*/
	module.exports.archiveProduct = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		console.log(userInformation);

		const productId = request.params.productId;

			let archivedProduct = {
				isActive: false
			}

		if(userInformation.isAdmin){
			return Products.findByIdAndUpdate(productId, archivedProduct, {new: true}).then(product => {
				response.send(`The product (_id: ${productId}) is not available or out of stock.`);
			}).catch(error => {
				response.send(error);
			})
		} else{
			return response.send("Sorry, you don't have access to this page.");
		}		
	}


/*Unarchive a product using ID by Admin only
(isActive: true - product is available)*/
	module.exports.unarchiveProduct = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		console.log(userInformation);

		const productId = request.params.productId;

			let unarchivedProduct = {
				isActive: true
			}

		if(userInformation.isAdmin){
			return Products.findByIdAndUpdate(productId, unarchivedProduct, {new: true}).then(product => {
				response.send(`Stocks restored. The product (_id: ${productId}) is already available.`);
			}).catch(error => {
				response.send(error);
			})
		} else{
			return response.send("Sorry, you don't have access to this page.");
		}		
	}