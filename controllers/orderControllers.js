/*Dependencies*/
const bcrypt = require("bcrypt");

/*File directories*/
const auth = require("../auth");
const User = require("../models/User");
const Products = require("../models/Products");
const Orders = require("../models/Orders");


/*Add to cart orders*/
	module.exports.addToCart = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		const productId = request.params.productId;
		const userDetails = `${userInformation.firstName} ${userInformation.lastName}`;
		return Products.findById(productId).then(result => {
			
			if(!userInformation.isAdmin){
				
				if(result.isActive){

					let newOrder = new Orders ({
						            userId: userInformation.id,
						            customerName: userDetails,
						            productId,
						            productName: result.name,
						            productPrice: result.price,
						            quantity: request.body.quantity,
						            subtotal: result.price * request.body.quantity
						        });
					return newOrder.save().then(order => {
						console.log(order);
						response.send(`
							Customer Name: ${userDetails}
							User ID: ${userInformation.id}
							Product: ${result.name}
							Price: ${result.price}
							Quantity: ${request.body.quantity}
							Subtotal: ${newOrder.productPrice * request.body.quantity}
							`);
					}).catch(error => {
						console.log(error);
						response.send(error);
					})
				} else{
					return response.send("Item not available.")
				}
			} else{
				return response.send("You are an admin.")
			}
		}).catch(error => {
			console.log(error);
			response.send(error);
		})
	}


/*Check Cart: Check all the items in the cart (quantity of different products)*/
	module.exports.checkCart = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);

		if(!userInformation.isAdmin){
			return Orders.find({id: userInformation.id}).then(result => {
				return Orders.aggregate([
						{$match : {
							userId : userInformation.id
						}},
						{$group: {
							_id: "$productId",
							quantity: {$sum: "$quantity"},
							subtotal: {$sum: "$subtotal"}
						}}
					]).then(result => {
						response.send(result)
					}).catch(error => console.log(error))
			}).catch(error => console.log(error))
		} else{
			return response.send("This page is for customer only.")
		}
	}


/*Remove from cart*/
	module.exports.removeFromCart = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		const productId = request.params.productId;

		return Orders.deleteMany({_id: request.body.productId}).then(removed => {
				console.log(removed)
				response.send(`Product ${request.body.productId} has been removed from cart.`)
			}).catch(error => {
				console.log(error)
				response.send("Sorry, there was an error during checkout.")
			})
	}