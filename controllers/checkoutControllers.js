/*Dependencies*/
const bcrypt = require("bcrypt");

/*File directories*/
const auth = require("../auth");
const User = require("../models/User");
const Products = require("../models/Products");
const Orders = require("../models/Orders");
const Checkout = require("../models/Checkout");


/*Change status of orders*/
module.exports.changeOrderStatus = async (request, response, next) => {
	const userInformation = auth.decodeToken(request.headers.authorization);

	if(!userInformation.isAdmin){

		let checkoutItem = await Orders.find({id: userInformation.id}).updateMany({productId: request.params.productId}, {
			$set: {
				status: "Ready for checkout!"
			}
		}).then(result => result).catch(error => {
			console.log(error);
			response.send(error)})

		next();
	} else{
		return response.send("You are an admin. You don't have access to this page.")
	}
}


/*For checkout per product*/
module.exports.checkout = (request, response) => {
	const userInformation = auth.decodeToken(request.headers.authorization);

	if(!userInformation.isAdmin){
		return Orders.find({id: userInformation.id}).then(result => {
			return Orders.aggregate([
					{$match: {
						productId: request.params.productId
					}},
					{$group: {
						_id: "$productId",
						totalQuantity: {$sum: "$quantity"},
						totalAmount: {$sum: "$subtotal"}
					}}
				]).then(checkedOut => {
					console.log(checkedOut);

					let newCheckout = new Checkout(
							{
								userId: userInformation.id,
								deliveryAddress: request.body.deliveryAddress,
								productId: request.params.productId,
								totalQuantity: checkedOut[0].totalQuantity,
								totalAmount: checkedOut[0].totalAmount
							}
						)
					console.log(newCheckout);
					return newCheckout.save().then(saved => {
						response.send(`
								PRODUCT SUBTOTAL
								Delivery Address: ${saved.deliveryAddress}
								Product ID: ${saved.productId}
								Product Subtotal: ${checkedOut[0].totalAmount}
								Purchased On: ${saved.purchasedOn}
							`)
					}).catch(error => {
						console.log(error);
						response.send("Sorry, there was an error during checkout.");
					})
				})
		})
	} else response.send("You are an admin. You don't have access to this page.")
}


/*Grand total for all the items*/
module.exports.checkoutAll = (request, response) => {
	const userInformation = auth.decodeToken(request.headers.authorization);

	if(!userInformation.isAdmin){
		return Checkout.find({id: userInformation.id}).then(result => {
			return Checkout.aggregate([
					{$match: {
						userId: userInformation.id
					}},
					{$group: {
						_id: "$userId",
						totalAmount: {$sum: "$totalAmount"}
					}}
				]).then(saved => {
					console.log(saved);
					response.send(`
								Your GRAND TOTAL is: ${saved[0].totalAmount}
								Thank you for ordering!
							`)
				}).catch(error => {
					console.log(error);
					response.send(error);
				})
		}).catch(error => {
			console.log(error);
			response.send(error);
			})		
	} else response.send("You are an admin. You don't have access to this page.")
}