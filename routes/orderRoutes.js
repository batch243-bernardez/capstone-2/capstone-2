/*Dependencies*/
const express = require("express");
const router = express.Router();

/*File directories*/
const auth = require("../auth")
const orderControllers = require("../controllers/orderControllers");

/*Check cart*/
router.get("/checkCart", auth.tokenVerification, orderControllers.checkCart);

/*Remove from cart*/
router.delete("/removeProductFromCart", auth.tokenVerification, orderControllers.removeFromCart)

/*Add to cart orders*/
router.post("/toPurchase/:productId", auth.tokenVerification, orderControllers.addToCart);

module.exports = router; 